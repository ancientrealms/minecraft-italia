package it.ancientrealms.minecraftitalia.command;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.electronwill.nightconfig.core.Config;

import dev.jorel.commandapi.CommandAPI;
import dev.jorel.commandapi.CommandAPICommand;
import it.ancientrealms.minecraftitalia.MinecraftItalia;
import it.ancientrealms.minecraftitalia.object.ApiServerInfo.UserVote;
import it.ancientrealms.minecraftitalia.object.Vote;

public final class VoteCommand
{
    private static final MinecraftItalia plugin = MinecraftItalia.getInstance();

    public static void register()
    {
        new CommandAPICommand("vote")
                .withPermission("minecraftitalia.command.vote")
                .withAliases("arvote")
                .executesPlayer((player, args) -> {
                    if (!plugin.getManager().isEnabled())
                    {
                        CommandAPI.fail("Il comando è disabilitato.");
                    }

                    if (plugin.getManager().isUpdating())
                    {
                        CommandAPI.fail("Stiamo aggiornando i voti. Attendi qualche istante.");
                    }

                    final UUID playeruuid = player.getUniqueId();
                    final Config redeemed = plugin.getPlayers().get("redeemed.%s".formatted(playeruuid));

                    final List<Vote> playervotes = new ArrayList<>();

                    if (redeemed != null)
                    {
                        redeemed.entrySet()
                                .stream()
                                .filter(e -> ((boolean) e.getValue()) == false)
                                .forEach(e -> {
                                    final Vote vote = new Vote("redeemed.%s.%s".formatted(playeruuid, e.getKey()), false);
                                    playervotes.add(vote);
                                });
                    }

                    final Optional<UserVote> vote = plugin.getManager().getUserVote(playeruuid);

                    if (!vote.isEmpty())
                    {
                        final String votepath = "redeemed.%s.%s".formatted(playeruuid, vote.get().getDate());

                        if (plugin.getPlayers().get(votepath) == null)
                        {
                            playervotes.add(new Vote(votepath, false));
                        }
                    }

                    if (playervotes.isEmpty())
                    {
                        CommandAPI.fail("Non hai premi da riscattare!");
                    }

                    playervotes.forEach(v -> {
                        plugin.getPlayers().set(v.getPath(), true);
                        plugin.getServer().dispatchCommand(plugin.getServer().getConsoleSender(), "crates givekey %s vote".formatted(player.getName()));
                    });

                    player.sendMessage("§aGrazie per il tuo sostegno!");
                })
                .register();
    }
}
