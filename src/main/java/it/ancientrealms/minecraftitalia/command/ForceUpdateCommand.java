package it.ancientrealms.minecraftitalia.command;

import dev.jorel.commandapi.CommandAPI;
import dev.jorel.commandapi.CommandAPICommand;
import dev.jorel.commandapi.arguments.LiteralArgument;
import it.ancientrealms.minecraftitalia.MinecraftItalia;
import it.ancientrealms.minecraftitalia.runnable.UpdateVotes;

public final class ForceUpdateCommand
{
    private static final MinecraftItalia plugin = MinecraftItalia.getInstance();

    public static void register()
    {
        new CommandAPICommand("vote")
                .withPermission("minecraftitalia.command.forceupdate")
                .withAliases("arvote")
                .withArguments(new LiteralArgument("forceupdate"))
                .executes((sender, args) -> {
                    if (!plugin.getManager().isEnabled())
                    {
                        CommandAPI.fail("Il comando è disabilitato.");
                    }

                    if (plugin.getManager().isUpdating())
                    {
                        CommandAPI.fail("C'è già un aggiornamento in corso...");
                    }

                    new UpdateVotes(true).runTaskAsynchronously(plugin);
                    sender.sendMessage("§aRichiesta inviata. Tra qualche istante dovrebbe finire.");
                })
                .register();
    }
}
