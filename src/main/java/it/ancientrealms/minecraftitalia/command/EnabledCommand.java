package it.ancientrealms.minecraftitalia.command;

import dev.jorel.commandapi.CommandAPICommand;
import dev.jorel.commandapi.arguments.BooleanArgument;
import dev.jorel.commandapi.arguments.LiteralArgument;
import it.ancientrealms.minecraftitalia.MinecraftItalia;

public final class EnabledCommand
{
    private static final MinecraftItalia plugin = MinecraftItalia.getInstance();

    public static void register()
    {
        new CommandAPICommand("vote")
                .withPermission("minecraftitalia.command.enabled")
                .withAliases("arvote")
                .withArguments(new LiteralArgument("enabled"))
                .withArguments(new BooleanArgument("status;bool"))
                .executes((sender, args) -> {
                    final boolean bool = (boolean) args[0];
                    final String msg = "Il comando /vote è stato: %s";

                    plugin.getManager().setEnabled(bool);
                    sender.sendMessage(msg.formatted(bool ? "§aattivato" : "§4disattivato"));
                })
                .register();
    }
}
