package it.ancientrealms.minecraftitalia.command;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import dev.jorel.commandapi.CommandAPI;
import dev.jorel.commandapi.CommandAPICommand;
import dev.jorel.commandapi.arguments.BooleanArgument;
import dev.jorel.commandapi.arguments.LiteralArgument;
import dev.jorel.commandapi.arguments.StringArgument;
import it.ancientrealms.minecraftitalia.MinecraftItalia;

public final class RegisterCommand
{
    private static final MinecraftItalia plugin = MinecraftItalia.getInstance();

    public static void register()
    {
        final Map<String, UUID> current = new HashMap<>();

        new CommandAPICommand("vote")
                .withPermission("minecraftitalia.command.register")
                .withAliases("arvote")
                .withArguments(new LiteralArgument("register"))
                .withArguments(new StringArgument("username;string").replaceSuggestions(info -> {
                    current.clear();
                    List.of(plugin.getServer().getOfflinePlayers()).forEach(p -> current.put(p.getName(), p.getUniqueId()));

                    return current.keySet().toArray(String[]::new);
                }))
                .withArguments(new StringArgument("date;YYYY-MM-DD").replaceSuggestions(info -> {
                    final DateTimeFormatter dateformatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                    final LocalDate today = LocalDate.now();
                    return new String[] { dateformatter.format(today) };
                }))
                .withArguments(new BooleanArgument("rewarded;bool"))
                .executes((sender, args) -> {
                    final String username = (String) args[0];
                    final String date = (String) args[1];
                    final boolean rewarded = (boolean) args[2];

                    if (!current.containsKey(username))
                    {
                        CommandAPI.fail("Utente non trovato.");
                    }

                    final UUID uuid = current.get(username);

                    try
                    {
                        final DateTimeFormatter dateformatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                        final LocalDate dateparse = LocalDate.parse(date);
                        final String datestring = dateformatter.format(dateparse);

                        if (!date.equals(datestring))
                        {
                            CommandAPI.fail("Formato non valido. Deve essere in formato YYYY-MM-DD.");
                        }

                        final String playerpath = "redeemed.%s.%s".formatted(uuid.toString(), datestring);
                        final String fmtmsg = rewarded ? "riscattato" : "non riscattato";

                        if (plugin.getPlayers().get(playerpath) != null)
                        {
                            plugin.getPlayers().set(playerpath, rewarded);
                            final String msg = "§eVoto in data §7%s §eaggiornato come: §7%s".formatted(datestring, fmtmsg);
                            sender.sendMessage(msg);
                            return;
                        }

                        plugin.getPlayers().set(playerpath, rewarded);
                        sender.sendMessage("§aVoto in data §7%s §aregistrato come: §7%s".formatted(datestring, fmtmsg));
                    }
                    catch (DateTimeParseException e)
                    {
                        CommandAPI.fail("Formato non valido. Deve essere in formato YYYY-MM-DD.");
                    }
                })
                .register();
    }
}
