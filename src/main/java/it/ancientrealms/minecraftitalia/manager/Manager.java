package it.ancientrealms.minecraftitalia.manager;

import java.util.Optional;
import java.util.UUID;

import it.ancientrealms.minecraftitalia.MinecraftItalia;
import it.ancientrealms.minecraftitalia.object.ApiServerInfo;
import it.ancientrealms.minecraftitalia.object.ApiServerInfo.UserVote;

public final class Manager
{
    private final MinecraftItalia plugin = MinecraftItalia.getInstance();
    private ApiServerInfo apiServerInfo;
    private boolean updating = false;
    private boolean enabled = true;

    public Manager(boolean enabled)
    {
        this.enabled = enabled;
    }

    public void updateApiServerInfo(ApiServerInfo apiServerInfo)
    {
        this.apiServerInfo = apiServerInfo;
    }

    public Optional<UserVote> getUserVote(UUID uuid)
    {
        return this.apiServerInfo
                .getUserVotes()
                .stream()
                .filter(u -> u.getUUID() != null)
                .filter(u -> u.getUUID().equals(uuid.toString()))
                .findFirst();
    }

    public boolean needsRefresh()
    {
        return this.apiServerInfo == null;
    }

    public void setUpdating(boolean bool)
    {
        this.updating = bool;
    }

    public boolean isUpdating()
    {
        return this.updating;
    }

    public void setEnabled(boolean bool)
    {
        plugin.getFileConfig().set("general.enabled", bool);
        this.enabled = bool;
    }

    public boolean isEnabled()
    {
        return this.enabled;
    }
}
