package it.ancientrealms.minecraftitalia.runnable;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.bukkit.scheduler.BukkitRunnable;

import com.electronwill.nightconfig.core.Config;
import com.electronwill.nightconfig.core.Config.Entry;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import it.ancientrealms.minecraftitalia.MinecraftItalia;
import it.ancientrealms.minecraftitalia.object.ApiError;
import it.ancientrealms.minecraftitalia.object.ApiServerInfo;

public final class UpdateVotes extends BukkitRunnable
{
    private final MinecraftItalia plugin = MinecraftItalia.getInstance();
    private final Gson gson = new GsonBuilder().serializeNulls().create();
    private final boolean forced;

    public UpdateVotes(boolean forced)
    {
        this.forced = forced;
    }

    @Override
    public void run()
    {
        final String serverslug = plugin.getFileConfig().get("general.server_slug");
        final String apikey = plugin.getFileConfig().get("general.api_key");

        if (serverslug.isEmpty() || apikey.isEmpty())
        {
            return;
        }

        final int today = LocalDate.now().getDayOfYear();
        final boolean flag1 = today != plugin.getPlayers().getInt("lastCheckTime");

        if (forced || flag1)
        {
            if (flag1)
            {
                plugin.getPlayers().set("lastCheckTime", today);

                final DateTimeFormatter dateformatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                final String todaystring = dateformatter.format(LocalDate.now());
                final String yesterdaystring = dateformatter.format(LocalDate.now().minusDays(1));

                final Set<? extends Entry> playeruuid = ((Config) plugin.getPlayers().get("redeemed")).entrySet();
                final List<String> removepaths = new ArrayList<>();

                playeruuid.forEach(i -> {
                    final String uuid = i.getKey();
                    final Set<? extends Entry> config = ((Config) i.getValue()).entrySet();

                    config.forEach(c -> {
                        final String date = c.getKey();
                        final boolean rewarded = c.getValue();
                        final boolean flag2 = !rewarded
                                || date.equals(todaystring)
                                || date.equals(yesterdaystring);

                        if (flag2)
                        {
                            return;
                        }

                        removepaths.add("redeemed.%s.%s".formatted(uuid, date));
                    });
                });

                removepaths.forEach(p -> plugin.getPlayers().remove(p));
            }

            plugin.getManager().setUpdating(true);
            plugin.getLogger().info("Scarico nuovi voti da Minecraft ITALIA...");

            try
            {
                final HttpRequest httprequest = HttpRequest.newBuilder()
                        .uri(new URI("https://api.minecraft-italia.it/v5/server-info/%s?key=%s".formatted(serverslug, apikey)))
                        .GET()
                        .build();
                final HttpClient httpclient = HttpClient.newHttpClient();

                final HttpResponse<String> httpresponse = httpclient.send(httprequest, HttpResponse.BodyHandlers.ofString());

                if (httpresponse.statusCode() >= 400)
                {
                    final Gson gson = new Gson();
                    final ApiError apierror = gson.fromJson(httpresponse.body(), ApiError.class);

                    plugin.getLogger()
                            .severe("Status: %s | Error: %s | Message: %s".formatted(apierror.getStatusCode(), apierror.getError(), apierror.getMessage()));
                    return;
                }

                final ApiServerInfo serverinfo = gson.fromJson(httpresponse.body(), ApiServerInfo.class);

                plugin.getPlayers().set("userVotes", gson.toJson(serverinfo));
                plugin.getManager().updateApiServerInfo(serverinfo);

                plugin.getLogger().info("Scaricamento completato.");
                plugin.getManager().setUpdating(false);
            }
            catch (URISyntaxException | IOException | InterruptedException e)
            {
            }
        }

        if (plugin.getManager().needsRefresh())
        {
            final String uservotes = plugin.getPlayers().get("userVotes");
            final ApiServerInfo serverinfo = gson.fromJson(uservotes, ApiServerInfo.class);
            plugin.getManager().updateApiServerInfo(serverinfo);
        }
    }
}
