package it.ancientrealms.minecraftitalia.object;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;

public final class ApiServerInfo
{
    private long lastVotesUpdate;
    private List<UserVote> userVotes;

    public long getLastVotesUpdate()
    {
        return this.lastVotesUpdate;
    }

    public List<UserVote> getUserVotes()
    {
        return this.userVotes;
    }

    public class UserVote
    {
        private String minecraftUUID;

        public String getUUID()
        {
            return this.minecraftUUID;
        }

        public String getDate()
        {
            try
            {
                final DateTimeFormatter dateformatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                final LocalDate yesterday = LocalDate.now().minusDays(1);
                final String datestring = dateformatter.format(yesterday);
                return datestring;
            }
            catch (DateTimeParseException e)
            {
                return "";
            }
        }
    }
}
