package it.ancientrealms.minecraftitalia.object;

public final class ApiError
{
    private int statusCode;
    private String message;
    private String error;

    public int getStatusCode()
    {
        return this.statusCode;
    }

    public String getMessage()
    {
        return this.message;
    }

    public String getError()
    {
        return this.error;
    }
}
