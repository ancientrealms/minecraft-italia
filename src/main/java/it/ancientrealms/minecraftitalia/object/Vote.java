package it.ancientrealms.minecraftitalia.object;

public final class Vote
{
    private final String path;
    private final boolean rewarded;

    public Vote(String path, boolean rewarded)
    {
        this.path = path;
        this.rewarded = rewarded;
    }

    public String getPath()
    {
        return this.path;
    }

    public boolean isRewarded()
    {
        return this.rewarded;
    }
}
