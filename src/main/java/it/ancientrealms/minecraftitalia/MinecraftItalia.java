package it.ancientrealms.minecraftitalia;

import java.io.File;

import org.bukkit.plugin.java.JavaPlugin;

import com.electronwill.nightconfig.core.file.FileConfig;

import it.ancientrealms.minecraftitalia.command.EnabledCommand;
import it.ancientrealms.minecraftitalia.command.ForceUpdateCommand;
import it.ancientrealms.minecraftitalia.command.RegisterCommand;
import it.ancientrealms.minecraftitalia.command.VoteCommand;
import it.ancientrealms.minecraftitalia.manager.Manager;
import it.ancientrealms.minecraftitalia.runnable.UpdateVotes;

public final class MinecraftItalia extends JavaPlugin
{
    private static MinecraftItalia instance;
    private FileConfig config;
    private FileConfig messages;
    private FileConfig players;
    private Manager manager;

    @Override
    public void onEnable()
    {
        instance = this;

        this.config = this.initFileConfig("config.toml");
        this.messages = this.initFileConfig("messages.toml");
        this.players = this.initFileConfig("players.json");

        this.manager = new Manager(this.config.get("general.enabled"));

        if (((String) this.config.get("general.server_slug")).isEmpty())
        {
            this.getLogger().severe("`server_slug` non può essere vuoto!");
        }

        if (((String) this.config.get("general.api_key")).isEmpty())
        {
            this.getLogger().severe("`api_key` non può essere vuoto!");
        }

        VoteCommand.register();
        RegisterCommand.register();
        ForceUpdateCommand.register();
        EnabledCommand.register();

        new UpdateVotes(false).runTaskTimerAsynchronously(this, 0, 20);
    }

    @Override
    public void onDisable()
    {
    }

    public static MinecraftItalia getInstance()
    {
        return instance;
    }

    public FileConfig getPlayers()
    {
        return this.players;
    }

    public FileConfig getFileConfig()
    {
        return this.config;
    }

    public FileConfig getFileMessages()
    {
        return this.messages;
    }

    public Manager getManager()
    {
        return this.manager;
    }

    private FileConfig initFileConfig(String resourcePath)
    {
        this.saveResource(resourcePath, false);
        final FileConfig config = FileConfig.builder(new File(this.getDataFolder(), resourcePath))
                .autoreload()
                .autosave()
                .build();
        config.load();
        return config;
    }
}
